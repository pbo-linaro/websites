<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.13";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.13</h1>
<pre>
Summary           : Multiple vulnerabilities fixed in VLC media player
Date              : April 2021
Affected versions : VLC media player 3.0.12 and earlier
ID                : VideoLAN-SB-VLC-3013
</pre>

<h2>Details</h2>
<p>A remote user could create a specifically crafted file that could trigger some various issues.</p>
<p>It is possible to trigger a remote code execution through a specifically crafted playlist, and tricking the user into interracting with that playlist elements.</p>
<p>This is explained in more details on the <a href="https://positive.security/blog/url-open-rce">reporter's article</a></p>
<p>It is also possible to trigger read or write buffer overflows with some crafted files or by a MITM attack on the automatic updater</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or 
remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through these vulnerability</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.13</b> addresses the issue.
</p>

<h2>Credits</h2>
<p>The playlist based RCE was reported by Fabian Bräunlein and Lukas Euler from positive.security</p>
<p>The AV1 in MP4 buffer overflow was reported by Zhen Zhou, NSFOCUS Security Team</p>
<p>The invalid free() in the kate decoder was reported by Jordan Milne</p>
<p>The updater system buffer overflow was reported by Fabian Yamaguchi</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
