<?php

$SPONSORS = array(
    array (
        "imgSrc"    => "VideoUnified.png",
        "link"      => "https://www.video-unified.com/"
    )
    ,
    array (
        "imgSrc"    => "abematv.png",
        "link"      => "https://abema.tv/"
    )
    ,
    array (
        "imgSrc"    => "jstream.jpg",
        "link"      => "https://www.stream.co.jp/english/"
    )
    ,
    array (
        "imgSrc"    => "IIJ.jpg",
        "link"      => "https://www.iij.ad.jp/en/"
    )
);

function getSponsors() {
    global $SPONSORS;
    return $SPONSORS;
}
?>
